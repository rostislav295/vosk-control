# vosk-control

A simple speech/intent recognition framework based on the Vosk API. Should be able to run on any GNU/Linux system (including ARM based devices such as Raspberry Pi). Also tested and working on macOS, though some of intents might not work as they call Linux specific shell commands.

## Installation

### Base Dependencies

`git`, `python`, `pip`

### Basic

This is the base version that runs entirely offline.
```
git clone https://codeberg.org/rostislav295/vosk-control.git
cd vosk-control
python3 -m venv .
source ./bin/activate
pip3 install sounddevice vosk pyserial
```

### Advanced

This version implements chatbot functionality using the OpenAI API and a text to speech engine. Requires either *festival* or *espeak* to be installed as the text of speech engine. You'll also need to either add your API key as a system wide environment variable or put it in the `.env` file in the chat folder.
```
git clone https://codeberg.org/rostislav295/vosk-control.git
cd vosk-control
python3 -m venv .
source ./bin/activate
pip3 install sounddevice vosk pyserial python-dotenv openai 
```

## Running

Navigate to directory you installed program and then run:

```
source ./bin/activate
python3 app/app.py
```
Alternatively you can run it from any directory using the appropriate absolute paths:
```
/path/to/bin/python3 /path/to/app/app.py
```

## Customizing

New voice commands can be added by editing the `app/intents.json` file. For example, you could create a command to launch Firefox like this:

```
"launch firefox": {
      "action": "shell",
      "params": ["firefox"]
    }
```
The 'action' attribute specifies which function to call (as defined in the `app/actions.py` file), and the 'params' attribute specifies what parameters to pass in to the function. If a function requires multiple parameters you can use `"params": ["first_parameter", "second_parameter", "etc..."]`. The above example is equivalent to `shell("firefox")`, which executes `firefox` as a shell command.

