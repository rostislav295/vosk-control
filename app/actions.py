import subprocess
import shlex
import os
import json
import string
import time
import formatting
import scope

project_path = os.path.dirname(os.path.abspath(__file__))
current_directory_name = os.path.split(os.getcwd())[1] 
parent_path = os.path.abspath(__file__ + "/../../")
pyt = f"{parent_path}/bin/python3"

def sleep():
    try:
        # play_sound("ok.wav")
        shell(f' python3 "{project_path}/arduino/led.py" 0')
        formatting.prPurple("Currently inactive...")
    except Exception as e:
        print("An error occurred:", e)

def wake():
    try:
        # play_sound("laser.wav")
        shell(f' python3 "{project_path}/arduino/led.py" 1')
        formatting.prYellow("Currently active...")
    except Exception as e:
        print("An error occurred:", e)

def keypress(key_combination):
    try:
        subprocess.run(["xdotool", "key", key_combination])
    except Exception as e:
        print("An error occurred:", e)

def insert(text_to_insert):
    try:
        subprocess.run(["xdotool", "type", text_to_insert])
    except Exception as e:
        print("An error occurred:", e)

def shell(command):
    try:
        subprocess.run(shlex.split(command))
    except Exception as e:
        print("An error occurred:", e)

def play_sound(sound):
    try:
        shell(f"ffplay -v 0 -nodisp -autoexit {project_path}/sounds/{sound}")
        print(project_path)
    except Exception as e:
        print("An error occurred:", e)

def chat(input, sp=False):
    try:
        scope.mode_state = "command"
        if sp:
            command = [pyt, f"{project_path}/chat/chat.py", add_punctuation(input), sp]
        else:
            command = [pyt, f"{project_path}/chat/chat.py", add_punctuation(input)]
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()

        if process.returncode == 0:
            response_text = output.decode("utf-8").strip()
            formatting.prLightPurple(f'{response_text}')

            with open(f'{project_path}/chat/test_recording.txt', "w") as file:
                file.write(f"{response_text}")

            time.sleep(0.2)
            shell(f"espeak -f '{project_path}/chat/test_recording.txt'")
            
        else:
            print("Error:", error.decode("utf-8"))
        
    except Exception as e:
        print("An error occurred:", e)
    
def chat1(input, sp=False):
    try:
        scope.mode_state = "command"
        if sp:
            command = [pyt, f"{project_path}/chat/chat1.py", add_punctuation(input), sp]
        else:
            command = [pyt, f"{project_path}/chat/chat1.py", add_punctuation(input)]
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()

        if process.returncode == 0:
            response_text = output.decode("utf-8").strip()
            formatting.prLightPurple(f'{response_text}')

            with open(f'{project_path}/chat/test_recording.txt', "w") as file:
                file.write(f"{response_text}")

            time.sleep(0.2)
            shell(f"espeak -f '{project_path}/chat/test_recording.txt'")
            
        else:
            print("Error:", error.decode("utf-8"))
        
    except Exception as e:
        print("An error occurred:", e)


def chat2(input):
    try:
        scope.mode_state = "command"
        command = [pyt, f"{project_path}/chat/chat2.py", add_punctuation(input)]
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()

        if process.returncode == 0:
            response_text = output.decode("utf-8").strip()
            formatting.prLightPurple(f'{response_text}')

            with open(f'{project_path}/chat/test_recording.txt', "w") as file:
                file.write(f"{response_text}")

            time.sleep(0.2)
            
        else:
            print("Error:", error.decode("utf-8"))
            subprocess.run(["espeak", "Oh no, I'm out of memory. Please start a new chat"])
        
    except Exception as e:
        print("An error occurred:", e)



def send_serial(data):
    shell(f' python3 "{project_path}/arduino/led.py" "{data}"')
        

    
# Read conversation history from file
def read_conversation_history(filename):
    try:
        with open(filename, 'r') as file:
            history = json.load(file)
        print(history)
    except Exception as e:
        print("An error occurred:", e)
    

# Write conversation history to file
def write_conversation_history(filename, history):
    try:
        with open(filename, 'w') as file:
            json.dump(history, file)
        print(history)
    except Exception as e:
        print("An error occurred:", e)

def update_program():
    try:
        temp = "temp"
        repo_url = "https://codeberg.org/rostislav295/vosk-control.git"
        subprocess.run(f'git clone {repo_url} "{parent_path}/{temp}"', shell=True)
        subprocess.run(f"mv '{project_path}/chat/.env' '{parent_path}'")
        subprocess.run(f'rm -rf "{project_path}/"* && rsync -r "{parent_path}/{temp}/app/" "{project_path}/"', shell=True)
        subprocess.run(f'rm -rf "{parent_path}/{temp}"', shell=True)
        subprocess.run(f"mv '{parent_path}/.env' {project_path}/chat.env")
        subprocess.Popen([pyt, f"{project_path}/app.py"])
        exit()
    except Exception as e:
        print("An error occurred:", e)

def restart_program():
    try:
        subprocess.Popen([pyt, f"{project_path}/app.py"])
        exit()
    except Exception as e:
        print("An error occurred:", e)




def add_punctuation(text):
    # Remove leading/trailing whitespace and convert to lowercase
    text = text.strip().lower()

    # Check if text ends with a punctuation mark
    if text[-1] in string.punctuation:
        return text

    # Detect the appropriate punctuation mark based on context
    if text.startswith(('?', 'who', 'what', 'where', 'when', 'why', 'how', 'are')):
        return text + '?'
    else:
        return text + '.'