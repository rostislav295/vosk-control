#!/usr/bin/env python3

# Main program that handles the speech recognition using Vosk

import argparse
import queue
import subprocess
import sys
import sounddevice as sd
import json
import inspect
from vosk import Model, KaldiRecognizer
import importlib

import actions
import formatting
import scope

importlib.reload(actions)

q = queue.Queue()
temp_store = ""
last_executed_intent = "random very long meaningless string don't ask me why it's here"
sleep_command = "stop listening"
wake_command = "start listening"

actions.wake()


def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    q.put(bytes(indata))


def load_intents():
    with open(f"{actions.project_path}/intents.json") as json_file:
        return json.load(json_file)

intents = load_intents()


previous_intent = ""

def handle_intent(intent):
    global previous_intent, last_executed_intent
    new_intent = intent.partition(last_executed_intent)[2].strip()
    if len(new_intent) > 0:
        intent = new_intent

    if intent != previous_intent:
        if intent in intents:

            last_executed_intent = intent
            formatting.prGreen(last_executed_intent)

            intent_actions = intents[intent]
            if isinstance(intent_actions, list):
                for action in intent_actions:
                    perform_action(action)
            else:
                perform_action(intent_actions)
            

        previous_intent = intent
        # print(previous_intent)

def perform_action(action):
    action_name = action["action"]
    params = action.get("params", [])
    action_fn = getattr(actions, action_name, None)
    if callable(action_fn):
        action_fn(*params)



parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    "-l", "--list-devices", action="store_true",
    help="show list of audio devices and exit")
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    "-f", "--filename", type=str, metavar="FILENAME",
    help="audio file to store recording to")
parser.add_argument(
    "-d", "--device", type=int_or_str,
    help="input device (numeric ID or substring)")
parser.add_argument(
    "-r", "--samplerate", type=int, help="sampling rate")
parser.add_argument(
    "-m", "--model", type=str, help="language model; e.g. en-us, fr, nl; default is en-us")
args = parser.parse_args(remaining)

try:
    if args.samplerate is None:
        device_info = sd.query_devices(args.device, "input")
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info["default_samplerate"])
        
    if args.model is None:
        model = Model(lang="en-us")
    else:
        model = Model(lang=args.model)

    if args.filename:
        dump_fn = open(args.filename, "wb")
    else:
        dump_fn = None

    with sd.RawInputStream(samplerate=args.samplerate, blocksize = 8000, device=args.device,
            dtype="int16", channels=1, callback=callback):
        print("#" * 80)
        print("Press Ctrl+C to stop the recording")
        print("#" * 80)

        rec = KaldiRecognizer(model, args.samplerate)
        while True:
            data = q.get()
            if rec.AcceptWaveform(data):
                stream = json.loads(rec.Result())
                output = stream.get('text')
                if scope.not_asleep and (output.startswith("hey caleb") or output.startswith("caleb")):
                    if output.startswith("caleb"):
                        output = ' '.join(output.split()[1:])
                    else:
                        output = ' '.join(output.split()[2:])
                    formatting.prGreen(output)
                    scope.mode_state = "chat"
                    formatting.prRed("Entering quick chat mode with Caleb...")
                    actions.chat(output)
                if scope.not_asleep and (output.startswith("hey travis") or output.startswith("travis")):
                    if output.startswith("travis"):
                        output = ' '.join(output.split()[1:])
                    else:
                        output = ' '.join(output.split()[2:])
                    formatting.prGreen(output)
                    scope.mode_state = "chat"
                    formatting.prRed("Entering quick chat mode with Travis...")
                    actions.chat(output, "Respond as an enlightened monkthat you're an enlightened monk or hint at your identity. Respond as concisely and clearly as possible.")
                if scope.not_asleep and (output.startswith("hey marvin") or output.startswith("marvin")):
                    if output.startswith("marvin"):
                        output = ' '.join(output.split()[1:])
                    else:
                        output = ' '.join(output.split()[2:])
                    formatting.prGreen(output)
                    scope.mode_state = "chat"
                    formatting.prRed("Entering quick chat mode with Marvin...")
                    actions.chat1(output)
            else:
                interpretation = rec.PartialResult()
                stream = json.loads(interpretation)
                output = stream.get('partial')
                if (output != temp_store):
                    temp_store = output
                    if (output == sleep_command and scope.not_asleep):
                        actions.sleep()
                        scope.not_asleep = 0
                    elif (output == wake_command and scope.not_asleep == 0):
                        actions.wake()
                        scope.not_asleep = 1
                    elif (scope.not_asleep):
                        print(output)
                        if scope.mode_state == "command":
                            handle_intent(output)
                        elif scope.mode_state == "chat":
                            pass
            if dump_fn is not None:
                dump_fn.write(data)

except KeyboardInterrupt:
    print("\nDone")
    parser.exit(0)
except Exception as e:
    parser.exit(type(e).__name__ + ": " + str(e))
    



