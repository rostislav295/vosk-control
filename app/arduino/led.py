import serial
import time
import sys

# Replace 'COMX' with the actual serial port your Arduino is connected to
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)

# Wait for the Arduino to reset (if necessary)
time.sleep(2)

# Check for the correct number of command-line arguments
if len(sys.argv) != 2:
    print("Usage: python3 Caleb_v0.3.py [command]")
else:
    command = sys.argv[1]

    # Send the command directly to the Arduino
    ser.write(command.encode())

# Close the serial connection
ser.close()
