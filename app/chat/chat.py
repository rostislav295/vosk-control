import os
import sys
import time
import openai
import dotenv
dotenv.load_dotenv()

openai.api_key = os.getenv("OPENAI_API_KEY")

if openai.api_key is None:
    print("OPENAI_API_KEY environment variable not set")
    exit(1)

systemPrompt = "You are a personal robot who responds fairly concisely and in a funny way that is sometimes quite rude."
userInput = sys.argv[1]

prompt = f"{systemPrompt}\n{userInput}"

response = openai.completions.create(
    model="gpt-3.5-turbo-instruct",
    prompt=prompt,
    max_tokens=30,
    temperature=0.5
)

responseText = response.choices[0].text.strip()
print(responseText)