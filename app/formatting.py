import inspect


# Print With Colors
def prRed(skk): print("\033[91m{}\033[00m" .format(skk))
 
 
def prGreen(skk): print("\033[92m{}\033[00m" .format(skk))
 
 
def prYellow(skk): print("\033[93m{}\033[00m" .format(skk))
 
 
def prLightPurple(skk): print("\033[94m{}\033[00m" .format(skk))
 
 
def prPurple(skk): print("\033[95m{}\033[00m" .format(skk))
 
 
def prCyan(skk): print("\033[96m{}\033[00m" .format(skk))
 
 
def prLightGray(skk): print("\033[97m{}\033[00m" .format(skk))
 
 
def prBlack(skk): print("\033[98m{}\033[00m" .format(skk))

# Print With Position

def print_with_line(message):
    frame = inspect.currentframe().f_back
    line_num = frame.f_lineno
    module_name = inspect.getmodule(frame).__name__
    prLightGray(f"[{module_name}:{line_num}] {message}")